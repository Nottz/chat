import { Component, OnInit, Input } from '@angular/core';
import {MessageService} from "../message.service";

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  @Input() private id:number
  @Input() private name:number
  @Input() private content:number

  constructor(private messageService: MessageService) { }

  ngOnInit() {
  }



}
