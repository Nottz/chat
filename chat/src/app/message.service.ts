import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from "rxjs/Subject";

@Injectable()
export class MessageService {

  // IP Antho
  // 192.168.1.42

  private readonly API: string = 'http://192.168.1.42:8000';
  private messages: any[] = [];

  public messagesSubject: Subject<any>  = new Subject<any>()

  constructor(private http: HttpClient) { }

  emitMessages() {
    this.messagesSubject.next(this.messages);
  }

  // Récupère les messages

  getMessages() {
    this.http.get<any>(`${this.API}/messages`)
      .subscribe(
        (messages) => {
          this.messages = messages
          this.emitMessages();
        },
        error => { console.error('MessageService.getMessages(): ', error); },
        () => { console.info('MessageService.getMessages(): complete'); }
      );
  }

  // Ajoute un message

  /*public addMessage(message) {
    return this.http.post<{name}>(`${this.API}/message/new`).subscribe(
      data => {
        message.id = data.name
        this.messages.push(message)
        this.emitMessages()
      },
      error => console.error('MessageService.addMessage(): ', error),
      () => console.info('MessageService.addMessage(): complete')
    );
  }*/

}
