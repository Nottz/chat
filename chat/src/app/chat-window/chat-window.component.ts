import { Component, OnInit } from '@angular/core';
import {MessageService} from "../message.service";

@Component({
  selector: 'app-chat-window',
  templateUrl: './chat-window.component.html',
  styleUrls: ['./chat-window.component.scss']
})
export class ChatWindowComponent implements OnInit {

  private messages: any[] = []

  constructor(private messageService: MessageService) { }

  ngOnInit() {
    this.messageService.messagesSubject.subscribe(
      (messages) => this.messages = messages
    )
    this.messageService.getMessages();
  }

}
