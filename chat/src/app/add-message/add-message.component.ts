import { Component, OnInit } from '@angular/core';
import { NgForm } from "@angular/forms";
import { MessageService } from "../message.service";


@Component({
  selector: 'app-add-message',
  templateUrl: './add-message.component.html',
  styleUrls: ['./add-message.component.scss']
})
export class AddMessageComponent implements OnInit {

  constructor(private messageService: MessageService) { }

  ngOnInit() {
  }

  onSubmit(form : NgForm) {
    /*let message = form.value;
    this.messageService.addMessage(message)*/
  }

}
